const Listing = require("../models/Listings")
const User = require("../models/User")

module.exports.getSpecificListing = (reqParams) => {
	return Listing.findById(reqParams.listingId).then(result => {
		return result
	})
}

module.exports.getAllListings = () => {
	return Listing.find( {} ).then(result => {
		return result
	})
}

module.exports.getAllBids = () => {
	return Listing.find( {listingType: "bid"} ).then(result => {
		return result
	})
}

module.exports.getAllAsks = () => {
	return Listing.find( {listingType: "ask"} ).then(result => {
		return result
	})
}

module.exports.updateListing = (reqParams, reqBody) => {
	let updatedListing = {
		itemName: reqBody.itemName,
		colorWay: reqBody.colorWay,
		price: reqBody.price
	}
	return Listing.findByIdAndUpdate(reqParams.listingId, updatedListing).then((result, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}

module.exports.archiveListing = (reqParams, reqBody) => {
	let updatedListing = {
		status: "Inactive"
	}
	return Listing.findByIdAndUpdate(reqParams.listingId, updatedListing).then((result, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}