const User = require("../models/User.js")
// const Bid = require("../models/Bid.js")
// const Ask = require("../models/Ask.js")
const Listing = require("../models/Listings.js")
const Transaction = require("../models/Transaction.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")

module.exports.checkUserName = (req) => {
	return User.find({userName: req.userName}).then((res, error) => {
		if(error){
			console.log(error)
			return false
		}else{
			if(res.length > 0){
				return true
			}else{
				return false
			}
		}
	})
}

module.exports.registerUser = (req) => {
	let newUser = new User({
		userName: req.userName,
		firstName: req.firstName,
		lastName: req.lastName,
		age: req.age,
		location: req.location,
		gender: req.gender,
		email: req.email,
		password: bcrypt.hashSync(req.password, 10),
		mobileNo: req.mobileNo
	})
	return newUser.save().then((saved, error) => {
		if(error){
			console.log(error)
			return false
		}else{
			return true
		}
	})
}

// Check if user is existing
/*module.exports.noSameUser = (req, res, next) => {
	return User.findOne({userName: req.userName}).then(res => {
		if(res !== null){
			return "username is already in use"
		}else{
			next()
		}
	})
}*/

module.exports.userLogin = (req) => {
	return User.findOne({userName: req.userName}).then(res => {
		if(res === null){
			return false
		}else{
			const isPasswordCorrect = bcrypt.compareSync(req.password, res.password)
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(res.toObject())}
			}else{
				return false
			}
		}
	})
}

module.exports.getProfile = (req) => {
	return User.findById(req.userId).then(res => {
		if(res === null){
			return false
		}else{
			res.password = "";
			return res
		}
	})
}

module.exports.bid = async(data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.currentListings.push({
			listingType: "bid",
			itemName: data.itemName,
			colorWay: data.colorWay,
			price: data.price
			// {listingType: "bid"},
			// {itemName: data.body.itemName},
			// {colorWay: data.body.colorWay},
			// {price: data.body.price}

			})
		return user.save().then((user, error) => {
			if(error){
				return false
			}else{
				return true
			}
		})
	})
	let newListing = new Listing({
		listingType: "bid",
		itemName: data.itemName,
		colorWay: data.colorWay,
		price: data.price
	})
	return newListing.save().then((course, error) => {
		if(error){
			return false
		}else{
			return "New Bid Created"
		}
	})
/*
----THIS IS FOR CHECKING IF THERE ARE OTHER BIDS-----
	let is listingUpdated = await Listing.findById(data.listingsId).then(listing => {
		listing.push({
			listingType: "bid",
			itemName: req.body.itemName,
			colorWay: req.body.colorWay,
			price: req.body.price
		})
	})*/
	/*if(isUserUpdated && isListingUpdated){
		return true
	}else{
		return false
	}*/
}

module.exports.ask = async(data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.currentListings.push({
			listingType: "ask",
			itemName: data.itemName,
			colorWay: data.colorWay,
			price: data.price
			// {listingType: "bid"},
			// {itemName: data.body.itemName},
			// {colorWay: data.body.colorWay},
			// {price: data.body.price}

			})
		return user.save().then((user, error) => {
			if(error){
				return false
			}else{
				return true
			}
		})
	})
	let newListing = new Listing({
		listingType: "ask",
		itemName: data.itemName,
		colorWay: data.colorWay,
		price: data.price
	})
	return newListing.save().then((course, error) => {
		if(error){
			return false
		}else{
			return "New Ask Created"
		}
	})
}