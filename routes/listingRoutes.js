const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const listingController = require("../controllers/listingControllers.js")

router.get("/", (req, res) => {
	listingController.getAllListings().then(result => res.send(result))
})

router.get("/bids", (req, res) => {
	listingController.getAllBids().then(result => res.send(result))
})

router.get("/asks", (req, res) => {
	listingController.getAllAsks().then(result => res.send(result))
})

router.get("/:listingId", auth.verify, (req, res) => {
	listingController.getSpecificListing(req.params).then(result => res.send(result))
})

router.put("/:listingId", auth.verify, (req, res) => {
	listingController.updateListing(req.params, req.body).then(result => res.send(result))
})

router.put("/:listingId/archive", auth.verify, (req, res) => {
	listingController.archiveListing(req.params, req.body).then(result => res.send(result))
})

module.exports = router;