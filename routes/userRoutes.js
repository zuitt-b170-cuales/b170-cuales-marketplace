const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const userController = require("../controllers/userController.js")

// checking if code is working
router.get("/", (req, res) => {
	console.log("hello world, im testing u")
	res.send('hello im testing u')
})

// checking of username
router.post("/checkUserName", (req, res) => {
	userController.checkUserName(req.body).then(result => res.send(result))
})

// user registration
router.post("/register", /*userController.noSameUser,*/(req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
})

// user login
router.post("/login", (req, res) => {
	userController.userLogin(req.body).then(result => res.send(result))
})

// get profile
// --BROKEN, returns false even if true--- >>RESOLVED!
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile({userId: userData.id}).then(result => res.send(result))
})

// bid
router.post("/bid", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		itemName: req.body.itemName,
		colorWay: req.body.colorWay,
		price: req.body.price,
	}
	userController.bid(data).then(result => res.send(result))
})

// ask
router.post("/ask", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		itemName: req.body.itemName,
		colorWay: req.body.colorWay,
		price: req.body.price,
	}
	userController.ask(data).then(result => res.send(result))
})


module.exports = router;