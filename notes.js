// 0.5 Notes from youtube: JavaScript ES6 Arrow Functions Tutorial
// link: https://www.youtube.com/watch?v=h33Srr5J9nY
/*
	**EX1**
	function sum(a, b) {
		return a + b
	}

	
	let sum2 = (a, b) => {
		return a + b
	}

	**EX2**
	function isPositive(number){
		return number >= 0
	}

	let isPositive2 = (number) => {
		return number >= 0
	}

	**EX3**
	function randomNumber(){
		return Math.random
	}

	let randomNumber2 = () => {
		return Math.random
	}

	**EX4**
	document.addEventListener('click', function(){
		console.log('Click')
	})

	document.addEventListener('click', () => {
		console.log('Click')
	})

	**EX5**

*/

// 1. Notes from youtube: RESTful APIs in 100 seconds // Build an API from Scratch with Node.js Express
// link: https://www.youtube.com/watch?v=-MTSQjw5DrM

/*
	1. npm init -y
	2. npm install express
	3. index.js
		- declare app (import express)
		- const app = require('express')();
		- const PORT = 9080;
		- app.listen(PORT, () => {
			console.log(`Working now`)
		})
		- app.get('/tshirt', (req, res) => {
			res.status(200).send({
				tshirt: 'tee',
				size: 'large'
			})
		})
		** request is incoming data
		** response is response back to client
		- app.post ('/tshirt/:id', (req, res) => {
			const {id} = req.params;
			const {logo} = req.body;

			if (!logo) {
				res.status(418).send({message: `We need a logo`})
			}
			res.send({
				tshirt: `[HERE] with your ${logo}`
			})
		})
	4. 
*/

// 2. Notes from youtube: How to build a REST API with NodeJs and Express
// link: https://www.youtube.com/watch?v=pKd0Rpw7O48

/*
	1. Client - Server architecture 
		- most if not all applications use this, these days.
		- client = front end
		- server = back end
		- happens using http protocol
		- Client - http - Server
	2. REST
		- Representational State Transfer
		- used to Create, Read, Update, Delete (CRUD)
	3. Standard http methods
		- GET 
			- /api/customers
				- response: array of customers
			- /api/customers/1
				- response: specific customer
		- POST 
			- /api/customers
				- include: customer objects
				- response: new customer added
		- PUT
			- /api/customers/1
				- include: customer object in body request
				- response: updates specific customer
		- DELETE
			- /api/customers/1
				- response: deletes specific customer
	4. app.get('/', (req, res) => {
			res.send('Hello World');
		})

		'/' - path / url
		(req, res) => ... - route handler
	5.  
*/

// 3. Notes from youtube: Learn Express Middleware in 14 Minutes
// link: https://www.youtube.com/watch?v=lY6icfhap2o

/*
	1. What is middleware?
		- Function or program that is is going to run in the middle
		- Bridges the request and response
	2. Req, res, next
		- next is actually just a function so just call it after
	3. Global
		- app.use(logger)
		- this will run before every single request
	4. Middleware runs in order that it is defined 
*/

// YC
/*
	1. 1 or more are technical
	2. Founders know each other
	3. Progress over time
	4. What do you know about the space that others don't?
	5. Ability to communicate
*/