const express = require('express')
const mongoose = require("mongoose")
const cors = require("cors")

// access to routes
// const askRoutes = require("./routes/userRoutes")
// const bidRoutes = require("./routes/bidRoutes")
// const portfolioRoutes = require("./routes/portfolioRoutes")
// const transactionRoutes = require("./routes/transactionRoutes")
const userRoutes = require("./routes/userRoutes")
const listingRoutes = require("./routes/listingRoutes")

// server
const app = express()
const port = 8000

// testing
/*app.get('/', (req, res) => {
	res.send('Home Page')
})*/

// use
app.use(cors())
app.use(express.urlencoded({ extended: true }))
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use("/api/users", userRoutes)
app.use("/api/listings", listingRoutes)
// app.use("/api/ask", askRoutes)
// app.use("/api/bid", bidRoutes)
// app.use("/api/transaction", transactionRoutes)
// app.use("/api/portfolio", portfolioRoutes)

// mongoose.connect
mongoose.connect("mongodb+srv://owencuales:OplanSagittarius1972@wdc028-course-booking.3hxrc.mongodb.net/marketplacev1?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
	})

// db
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"))
// app.listen(process.env.port, () => console.log(`Now listening to port 8000`))
app.listen(port, () => console.log(`Now listening to ${port}`))

// Questions to ask:
/*
	1. is backend just a series of gets, posts, updates, and delets?
	2. How to put URI in schema?
	3. How to assign URI when signed up
	4. Do we create a new cluster or just a new db inside the WDC028-course-booking?
*/