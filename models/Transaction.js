const mongoose = require("mongoose")

const transactionSchema = new mongoose.Schema({
	transactionId: {
		type: String,
		default: ""
	},
	itemName: {
		type: String,
		default: ""
	},
	brandName: {
		type: String,
		default: ""
	},
	colorWay: {
		type: String,
		default: ""
	},
	transactionDate: {
		type: Date,
		default: new Date()
	},
	transactionPrice: {
		type: Number,
		default: ""
	},
	status: {
		type: String,
		default: "Inactive"
	}
})

module.exports = mongoose.model("Transaction", transactionSchema)