const mongoose = require("mongoose")

const askSchema = new mongoose.Schema({
	// item ID is supposedly URI
    listingId: {
        type: String,
        // assign a listing id
        default: ""
    },
    userNameOfAsker: {
        type: String,
        // changee userName to person posting
        default: ""
    },
	itemName: {
		type: String,
		required: [true, "Item Name is required"]
	},
	brandName: {
		type: String,
		required: [true, "Brand Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	colorWay: {
		type: String,
		required: [true, "Colorway is required"]
	},
	/*isItemVerified: {
		type: Boolean,
		default: false
	},*/
    addedOn: {
        type: Date,
        default: new Date()
    },
	status: {
		type: String,
		default: "Active"
		// for status: active, reserved, inactive
		// active - still on ask
		// reserved - matched but has to go through process
		// inactive - deal done or returned
	},
	size: {
		type: Number,
		required: [true, "Size is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	}
})

module.exports = mongoose.model("Ask", askSchema)