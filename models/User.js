const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	userName: {
        type: String,
        required: [true, "User Name is Required"]
    },
	firstName: {
		type: String,
		required: [true, "First Name is Required"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is Required"]
	},
	age: {
		type: String,
		required: [true, "Age is Required"]
	},
	location: {
		type: String,
		required: [true, "Location is Required"]
	},
	gender: {
		type: String,
		required: [true, "Gender is Required"]
	},
	email: {
		type: String,
		required: [true, "Email is Required"]
	},
	password: {
		type: String,
		required: [true, "Password is Required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is Required"]
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	isUserVerified: {
		type: Boolean,
		default: false
	},
	wallet: {
		type: Number,
		default: 0
	},
	commission: {
		type: Number,
		default: 0.15
	},
	currentListings: [
		{
            listingId: {
                type: String,
            }
        },
        {
        	listingType: {
        		type: String
        	}
        },
        {
            itemName: {
                type: String
            }
        },
        {
        	colorWay: {
        		type: String
        	}
        },
        {
        	price: {
        		type: Number
        	}
        }
	]
})


// ADD NEXT FEATURE: PORTFOLIOS
/*
	
	portfolio: [
		{
            itemId:{
                type: String,
                required: [true, "Course ID is required"]
            }
        },
        {
        	itemName: {
        		type: String,
        		required: [true, "Item Name is required"]
        	}
        },
        {
        	brandName: {
        		type: String,
        		required: [true, "Brand Name is required"]
        	}
        },
        {
        	description: {
        		type: String,
        		required: [true, "Brand Name is required"]
        	}
        },
        {
        	colorWay: {
        		type: String,
        		required: [true, "Colorway is required"]
        	}
        },
        {
        	isItemVerified: {
        		type: Boolean,
        		default: false
        	}
        },
        {
            addedOn: {
                type: Date,
                default: new Date()
            }
        },
        {
        	status: {
        		type: String,
        		default: "Active"
        		// for status: active, reserved, inactive
        	}
        },
        {
        	size: {
        		type: Number,
        		required: [true, "Size is required"]
        	}
        },
        {
        	price: {
        		type: Number,
        		required: [true, "Price is required"]
        	}
        }
	],

*/

module.exports = mongoose.model("User", userSchema)