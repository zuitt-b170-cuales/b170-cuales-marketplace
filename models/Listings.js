const mongoose = require("mongoose")

const listingSchema = new mongoose.Schema({
	listingType: {
		type: String
	},
	itemName: {
		type: String,
		required: [true, "Item Name is required"]
	},
	colorWay: {
		type: String,
		required: [true, "Color Way is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	status: {
		type: String,
		default: "Active"
	}
})

module.exports = mongoose.model("Listing", listingSchema)